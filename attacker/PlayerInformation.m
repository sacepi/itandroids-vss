load Gol1.txt

time = (Gol1(:,1) - Gol1(1,1))/1000;

playerNumber = 8;

playerPos.x = Gol1(:,3*playerNumber-1);
playerPos.y = Gol1(:,3*playerNumber);
playerPos.psi = Gol1(:,3*playerNumber+1);

tic
for index = 2:length(playerPos.x)
    auxtime = toc;
    figure(1);
    clf();
    axis([6,94,6,78])
    hold on
    plot(playerPos.x(index),playerPos.y(index),'.','MarkerSize',50)
    plot([playerPos.x(index) , playerPos.x(index)+3*cosd(playerPos.psi(index))],[playerPos.y(index),playerPos.y(index)+3*sind(playerPos.psi(index))],'k');
    
    drawnow;
    auxtime = toc - auxtime;
    auxtime = time(index) - time(index-1) - auxtime;
    if(auxtime<0) 
        auxtime = 0;
    end
    %pause(auxtime);
    name = sprintf('./video/frame%04d.jpg',index);    
    print(name,'-djpg');
end
toc
