%% Otimization algorithm to find the Bode plot of a DC motor and to find
% its transfer function. Here, we assume the transfer function is of 1st
% order, and we must also consider the discretization efect of the sine
% input, ie, it acts as a Zero Order Hold.

close all; clear;

%% Bode plot of DC motor
% Frequencies to be tested
f = [0.05 0.2 0.4 0.6 0.8 1:0.5:10];
w = 2*pi*f;
T = 1./f;

% Arduino timer and encoder configurations;
V0 = 4.85;   % PWM max tension
OCRA = 77;
Ts = 1/(16e6/1024/(OCRA+1));    % timer overflow
encoder_resolution = 600;   % ticks per revolution
pulses_to_rpm =  1/encoder_resolution/Ts*60;
%trials = round(20*T/Ts);

x = load('BodeData\LeftMotorBodeData.txt');

% Loop to find amplitude and phase of the output sine
for i = 1:length(f)
    frequency = f(i);   
    index = (x(:,1) == frequency); 
    vel = x(index,2);
    vel=[0; diff(vel)]*pulses_to_rpm;
    %vel=[0; diff(vel)]*pulses_to_rpm;
    speed = vel';
    times = (0:length(speed)-1)*Ts;
    
    init_guess = [800 -pi/10];
    
    fitted = fminsearch(...
        @(x) cost_fcn1(times, speed, frequency, x), init_guess);
%     fitted = fminsearch(...
%         @(x) cost_fcn1(times, speed, frequency, x), init_guess, ...
%         optimset('PlotFcns', @optimplotfval, 'MaxFunEvals', 100000));

    Amplitude(i) = fitted(1);
    Phase(i) = atan2(sin(fitted(2)),cos(fitted(2)));
    
    estimated_speed = Amplitude(i)*sin(2*pi*frequency*times + Phase(i));
    
%     if frequency > 0
%         figure;
%         plot(times, speed, 'r'); hold on;
%         plot(times, estimated_speed, 'b'); hold off;
%         pause
%         close all;
%     end

    %fprintf('Amplitude: %f\n Phase(degrees): %f\n', Amplitude(i), Phase(i)/pi*180)
end

%% Discovering Transfer Function
% Find first order system that better approximates the discovered bode plot
Amplitude = 20*log10(Amplitude/V0);

K = 102;
init_guess = [29]; 
fitted = fminsearch(...
    @(x) cost_fcn2(Amplitude, Phase, w, K, Ts, x), init_guess, ...
    optimset('PlotFcns', @optimplotfval, 'MaxFunEvals', 10000000));

a = fitted(1)
%K = fitted(2)
%T = fitted(2)

kt = T/Ts;
kt=2;
s = j*w;
% 1st order
estimated_amp = 20*log10(abs( (1-exp(-s*kt*Ts))./(s*kt*Ts) *K*a./(s+a) ));
estimated_phase = phase( (1-exp(-s*kt*Ts))./(s*kt*Ts) *K*a./(s+a) );
estimated_phase = atan2(sin(estimated_phase), cos(estimated_phase));

figure;
subplot(2,1,1)
semilogx(w, Amplitude); hold;
semilogx(w, estimated_amp, 'r');
title('Bode Plot Comparison: Actual and Estimated');
ylabel('Amplitude (dB)')
legend('Actual', '1st order approximation')
subplot(2,1,2)
semilogx(w, Phase/pi*180); hold;
semilogx(w, estimated_phase/pi*180, 'r');
ylabel('Phase (degrees)')
xlabel('Frequency (rad/s)')
legend('Actual', '1st order approximation')

% Show system found
zpk([],[-a],K*a)

%% Step Input Comparision
% Here, we will compare the step response of our estimated 1st order
% system with the actual step response taken from the DC motor.

% Plot step response from motor by averaging over the trials
x2 = load('BodeData\LeftMotorStepResponseData.txt');
trials = 10;
vel = 0;
for i=1:trials
    index = (x2(:,1) == i);
    aux = x2(index,2);
    aux = [0; diff(aux)]*pulses_to_rpm;
    vel = vel + aux;

end
vel = vel/trials;
times = (0:length(vel)-1)*Ts;

% Step response of the estimated 1st order tf
s = tf('s');
kt = 1;
sys = (1-exp(-s*kt*Ts))/(s*kt*Ts) *K*a/(s+a);
figure;
u = V0*ones(size(times));
y = lsim(sys,u,times);

% Plot data
plot(times, vel); hold;
plot(times,y,'r'); 
title('Step Input Comparison: Actual and Predicted Response')
legend('Actual Response', 'Predicted Response')
xlabel('Time (s)')
ylabel('Velocity (rpm)')

% Find the variance
var = 1/length(vel)*((y-vel)')*(y-vel)

%% Control (Speed)
% Here, we will compare the step response of our estimated 1st order
% system with the actual step response taken from the DC motor.

% Plot step response from motor by averaging over the trials
% x2 = load('BodeData\RightMotorControlData2.txt');
% trials = 30;
% vel = 0;
% for i=1:trials
%     index = (x2(:,1) == i);
%     aux = x2(index,2);
%     aux = [0; diff(aux)]*pulses_to_rpm;
%     vel = vel + aux;
% 
% end
% vel = vel/trials;
% times = (0:length(vel)-1)*Ts;
% 
% % Step response of the estimated 1st order tf
% s = tf('s');
% G = K*a/(s+a);
% Ga = c2d(G, Ts, 'zoh');
% Gf = feedback(0.117*Ga, 1);
% figure;
% u = 200*ones(size(times));
% y = lsim(Gf,u,times);
% 
% % Plot data
% plot(times, vel); hold;
% plot(times,y,'r'); 
% title('Step Input Comparison: Actual and Predicted Response')
% legend('Actual Response', 'Predicted Response')
% xlabel('Time (s)')
% ylabel('Velocity (rpm)')
% 
% % Find the variance
% var = 1/length(vel)*((y-vel)')*(y-vel)
%% Control (Position)
% Here, we will compare the step response of our estimated 1st order
% system with the actual step response taken from the DC motor.

% Plot step response from motor by averaging over the trials
% x2 = load('BodeData\RightMotorControlData3.txt');
% trials = 30;
% pos = 0;
% for i=1:trials
%     index = (x2(:,1) == i);
%     aux = x2(index,2)*0.6;
%     pos = pos + aux;
% end
% pos = pos/trials;
% times = (0:length(pos)-1)*Ts;
% 
% % Step response of the estimated 1st order tf
% s = tf('s');
% G = K*a/(s+a)/s*6;
% Ga = c2d(G, Ts, 'zoh');
% Gf = feedback(0.0436*Ga, 1);
% figure;
% u = 90*ones(size(times));
% y = lsim(Gf,u,times);
% 
% % Plot data
% plot(times, pos); hold;
% plot(times,y,'r'); 
% title('Step Input Comparison: Actual and Predicted Response')
% legend('Actual Response', 'Predicted Response')
% xlabel('Time (s)')
% ylabel('Velocity (rpm)')

%% Bode Finder using Step Input (1st order)
% We will try to estimate K and a

% init_guess = [K, a];
% fitted = fminsearch(...
%     @(x) cost_fcn3(vel, Ts, times, x), init_guess);
%     %optimset('PlotFcns', @optimplotfval, 'MaxFunEvals', 10000000));
% 
% K = fitted(1)
% a = fitted(2)
% 
% % Step response of the estimated 1st order tf
% s = tf('s');
% sys = (1-exp(-s*Ts))/(s*Ts) *K*a/(s+a);
% figure;
% u = 4.5*ones(size(times));
% y = lsim(sys,u,times);
% 
% % Plot data
% plot(times, vel); hold;
% plot(times,y,'r'); 
% title('Step Input Comparison: Actual and Predicted Response')
% legend('Actual Response', 'Predicted Response')
% xlabel('Time (s)')
% ylabel('Velocity (rpm)')
% 
% % Find the variance
% var = 1/length(vel)*((y-vel)')*(y-vel)
% 
% % Show system found
% zpk([],[-a],K*a)

%% Bode Finder using Step Input (2nd order)
% We will try to estimate K and a

% init_guess = [K, 50, 1e4];
% fitted = fminsearch(...
%     @(x) cost_fcn4(vel, Ts, times, x), init_guess,...
%     optimset('PlotFcns', @optimplotfval, 'MaxFunEvals', 10000000));
% 
% K = fitted(1)
% a = fitted(2)
% b = fitted(3)
% 
% % Step response of the estimated 1st order tf
% s = tf('s');
% sys = (1-exp(-s*Ts))/(s*Ts) *K*a*b/(s+a)/(s+b);
% figure;
% u = 4.5*ones(size(times));
% y = lsim(sys,u,times);
% 
% % Plot data
% plot(times, vel); hold;
% plot(times,y,'r'); 
% title('Step Input Comparison: Actual and Predicted Response')
% legend('Actual Response', 'Predicted Response')
% xlabel('Time (s)')
% ylabel('Velocity (rpm)')
% 
% % Find the variance
% var = 1/length(vel)*((y-vel)')*(y-vel)
% 
% % Show system found
% zpk([],[-a, -b],K*a*b)
% 
% s = j*w;
% % 1st order
% estimated_amp = 20*log10(abs( (1-exp(-s*Ts))./(s*Ts) *K*a*b./(s+a)./(s+b) ));
% estimated_phase = phase( (1-exp(-s*Ts))./(s*Ts) *K*a*b./(s+a)./(s+b) );
% estimated_phase = atan2(sin(estimated_phase), cos(estimated_phase));
% 
% figure;
% subplot(2,1,1)
% semilogx(w, Amplitude); hold;
% semilogx(w, estimated_amp, 'r');
% title('Bode Plot Comparison: Actual and Estimated');
% ylabel('Amplitude (dB)')
% legend('Actual', '1st order approximation')
% subplot(2,1,2)
% semilogx(w, Phase/pi*180); hold;
% semilogx(w, estimated_phase/pi*180, 'r');
% ylabel('Phase (degrees)')
% xlabel('Frequency (rad/s)')
% legend('Actual', '1st order approximation')

%% Comparison with data from http://ctms.engin.umich.edu/CTMS/index.php?example=MotorPosition&section=SystemModeling
% J = 3.2284E-6;
% b = 3.5077E-6;
% K = 0.0274;
% R = 4;
% L = 2.75E-6;
% s = tf('s');
% zpk( K/((J*s+b)*(L*s+R)+K^2) )

