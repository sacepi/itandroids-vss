function cost = cost_fcn1(times, speed, frequency, guess)
    
    Amplitude = guess(1);
    Phase = guess(2);
    w = 2*pi*frequency;
    
    estimated_speed = Amplitude*sin(w*times + Phase);
    
    % MSE
    cost = (estimated_speed - speed)*((estimated_speed - speed)');

end