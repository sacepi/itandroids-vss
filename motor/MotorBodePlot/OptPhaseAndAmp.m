% Otimization algorithm to find amplitude and phase of a sine
close all; clear;


f = [0.2 0.4 0.6 0.8 1:0.5:23];
T = 1./f;

OCRA = 77;
overflow = 1/(16e6/1024/(OCRA+1));    % timer overflow = 0.01 s 
encoder_resolution = 600;   % 600 ticks per revolution
pulses_to_rpm =  1/encoder_resolution/overflow*60;
trials = round(20*T/overflow);

x = load('BodeData\data.txt');

for i = 1:length(f)
    frequency = f(i);
    index = (x(:,1) == frequency); 
    vel = x(index,2);
    vel=[0; diff(vel)]*pulses_to_rpm;
    speed = vel';
    times = (0:length(speed)-1)*overflow;

    w = 2*pi*frequency;
    init_guess = [300 -pi/10];
    
    fitted = fminsearch(...
        @(x) cost_fcn1(times, speed, frequency, x), init_guess);
%     fitted = fminsearch(...
%         @(x) cost_fcn1(times, speed, frequency, x), init_guess, ...
%         optimset('PlotFcns', @optimplotfval, 'MaxFunEvals', 100000));

    Amplitude(i) = fitted(1);
    Phase(i) = atan2(sin(fitted(2)),cos(fitted(2)));
    
    estimated_speed = Amplitude(i)*sin(w*times + Phase(i));

%     figure;
%     plot(times, speed, 'r'); hold on;
%     plot(times, estimated_speed, 'b'); hold off;
%     pause
%     close all;
    %fprintf('Amplitude: %f\n Phase(degrees): %f\n', Amplitude(i), Phase(i)/pi*180)
end

figure;
subplot(2,1,1)
plot(log10(f), 20*log10(Amplitude));
subplot(2,1,2)
plot(log10(f), Phase/pi*180);