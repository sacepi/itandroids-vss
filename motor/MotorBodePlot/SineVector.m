% Gerador de um vetor que ser� usado para simular um seno usando pwm

% Frequ�ncias de teste
f = [0.05 0.2:0.2:0.8 1:0.5:10];
T = 1./f;

OCRA = 77;
overflow = 1/(16e6/1024/(OCRA+1));    % timer overflow = 0.01 s 
encoder_resolution = 600;   % 600 ticks per revolution
pulses_to_rpm =  1/encoder_resolution/overflow*60;

trials = round(15*T/overflow);

for i=1:length(f)
    if (i ~= length(f))
        fprintf('%f, ', f(i))
    else
        fprintf('%f};\n', f(i))
    end
end

for i=1:length(trials)
    if (i ~= length(trials))
        fprintf('%d, ', trials(i))
    else
        fprintf('%d};\n', trials(i))
    end
end

% trials = 0;
% j=1;
% i=1;
% pwm_l = [];
% while(trials < 30)
%     if(i < length(pwm))
%         if(pwm(i) > 0)
%             pwm_l(j) = pwm(i);
%         else
%             pwm_l(j) = pwm(i);
%         end
%         i=i+1;
%         j=j+1;
%     else
%         trials=trials+1;
%         i = 1;
%         if(pwm(i) > 0)
%             pwm_l(j) = pwm(i);
%         else
%             pwm_l(j) = pwm(i);
%         end
%         i=i+1;
%         j=j+1;
%     end
% end
% figure; plot(pwm_l)