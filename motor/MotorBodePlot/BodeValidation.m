%% Control (Position)
% Here, we will compare the step response of our estimated 1st order
% system with the actual step response taken from the DC motor.

close all; clear;

% Parameters
s = tf('s');
G = 3731.8/(s+36.59)/s*6;
K = 0.08;
Ts = 0.004992;
Vmax = 8.2;
Vmin = 0.2;

x2 = load('BodeData\LeftMotorControlData.txt');
trials = 10;
pos = 0;
for i=1:trials
    index = (x2(:,1) == i);
    aux = x2(index,2)*0.6;
    pos = pos + aux;
end
pos = pos/trials;
times = (0:length(pos)-1)*Ts;

sim('ControlModel');

% Plot data
plot(times, pos); hold;
plot(simout.Time,simout.Data,'r'); 
title('Step Input Comparison: Actual and Predicted Response')
legend('Actual Response', 'Predicted Response')
xlabel('Time (s)')
ylabel('Velocity (rpm)')