% Otimization algorithm to find the best transfer function that fits a bode
% plot. Data must be in an x matrix
close all; 

amplitude = 20*log10(x(:,1));
phi = x(:,2);
frequency = x(:,3);
w = 2*pi*frequency;


% 2nd order guess
%init_guess = [100 1000]; 

% 3rd order guess
init_guess = [100 1000 2000];   
%init_guess = [100 1000 2000]; 

fitted = fminsearch(...
    @(x) cost_fcn2(amplitude, phi, frequency, x), init_guess, ...
    optimset('PlotFcns', @optimplotfval, 'MaxFunEvals', 10000000));

a = fitted(1);
b = fitted(2);
c = fitted(3);



% 2nd order guess
% figure;
% estimated_amp = 20*log10(abs(88.5097*b./(-w.^2 + j*a*w + b)));
% estimated_phi = phase(88.5097*b./(-w.^2 + j*a*w + b))/pi*180;
% subplot(2,1,1);
% semilogx(w, amplitude);  hold on;
% semilogx(w, estimated_amp,'r')
% subplot(2,1,2)
% semilogx(w, phi); hold on;
% semilogx(w, estimated_phi,'r');
% sys = tf([88.5097*b], [1 a b])

% 3rd order guess
figure;
estimated_amp = 20*log10(abs(88.5097*c./(-j*w.^3 - a*w.^2 + j*b*w + c)));
estimated_phi = phase(88.5097*c./(-j*w.^3 - a*w.^2 + j*b*w + c))/pi*180;
subplot(2,1,1);
semilogx(w, amplitude);  hold on;
semilogx(w, estimated_amp,'r')
semilogx(w, amp2, 'g')
subplot(2,1,2)
semilogx(w, phi); hold on;
semilogx(w, estimated_phi,'r');
semilogx(w, phi2, 'g')
sys = tf([88.5097*c], [1 a b c])

%figure; bode(sys)


