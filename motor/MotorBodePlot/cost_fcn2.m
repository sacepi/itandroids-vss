function cost = cost_fcn2(Amplitude, Phase, w, K, Ts, guess)
    
     %K = guess(2);
    a = guess(1);
    %Ts = guess(2);
    
    s = j*w;
    % 1st order
    estimated_amp = 20*log10(abs( (1-exp(-s*Ts))./(s*Ts) *K*a./(s+a) ));
    estimated_phase = phase( (1-exp(-s*Ts))./(s*Ts) *K*a./(s+a) );
    estimated_phase = atan2(sin(estimated_phase), cos(estimated_phase));
    
    %weight1 = [2*ones(1,floor(length(Amplitude)/2)) ones(1,floor(length(Amplitude)/2)+1)];
    %weight2 = [ones(1,floor(length(Amplitude)/2)+1) 5*ones(1,floor(length(Amplitude)/2))];
    
    error_amp = (estimated_amp - Amplitude);
    error_phase = (estimated_phase - Phase);
    
    cost_amp = 2*(error_amp)*((error_amp)');
    cost_phase = (error_phase)*((error_phase)');
    
    cost = cost_amp + cost_phase;
    
    % MSE
   %cost = 1/length(w)*((estimated_amp - Amplitude)*((estimated_amp - Amplitude)')/4 + ...
    %    (estimated_phase - Phase)*((estimated_phase - Phase)')*8/pi);
   %cost = 1/length(w)*((estimated_phase - Phase)*((estimated_phase - Phase)'));
    %cost = 1/length(w)*((estimated_amp - Amplitude)*((estimated_amp - Amplitude)'));

end