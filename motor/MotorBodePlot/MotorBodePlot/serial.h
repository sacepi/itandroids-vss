/*
 * serial.h
 *
 * Created: 26/11/2014 00:16:40
 *  Author: igor
 */ 


#ifndef SERIAL_H_
#define SERIAL_H_

#include <avr/io.h>
#include <stdlib.h>

class Serial{
public:
	Serial();
	void init();
	void send_char(unsigned char ch);
	void send_str(char * str);
	void send_int32(int32_t n);
};

#endif /* SERIAL_H_ */