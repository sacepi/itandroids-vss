/*
 * MotorBodePlot.cpp
 *
 * Created: 28/03/2015 13:16:54
 *  Author: igor
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include <math.h>
#include "serial.h"

#define ChannelA_l (PINC & (1 << PINC0))
#define ChannelB_l (PINC & (1 << PINC1))
#define ChannelA_r (PINB & (1 << PINB1))
#define ChannelB_r (PINB & (1 << PINB2))
#define pwm_r OCR2A
#define pwm_l OCR2B
#define STBY  PORTD6
#define IN1_R PORTD7
#define IN2_R PORTB0
#define IN1_L PORTD5
#define IN2_L PORTD4
#define RED_LED   PORTC5
#define LED13	PORTB5


#define encoder_resolution 600 // 600 pulses per rotation
//#define timer_overflow_period 0.010048 // 10 ms
#define timer_overflow_period 0.004992 // 5 ms
#define pulses_to_rpm 20 // 1/encoder_resolution/timer_overflow_period*60

Serial serial;
char str[15];
int i = 0, j = 0, k = 0;
int pwm_aux = 0;
//Sine input
//double f[] = {0.050000, 0.200000, 0.400000, 0.600000, 0.800000, 1.000000, 1.500000, 2.000000, 2.500000, 3.000000, 3.500000, 4.000000, 4.500000, 5.000000, 5.500000, 6.000000, 6.500000, 7.000000, 7.500000, 8.000000, 8.500000, 9.000000, 9.500000, 10.000000};

//int trials[] = {15024, 7512, 5008, 3756, 3005, 3005, 2003, 1502, 1202, 1002, 859, 751, 668, 601, 546, 501, 462, 429, 401, 376, 354, 334, 316, 300};

//Step input
double f[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
int trials[] = {400, 400, 400, 400, 400, 400, 400, 400, 400, 400}; 
//double f[] = {1};
//int trials[] = {4000};


int vector_size = sizeof(f) / sizeof(f[0]);
const float p = 2*M_PI*timer_overflow_period;

// Encoder variables
volatile int32_t counter_l = 0;
volatile int32_t counter_r = 0;
volatile int32_t old_counter_l = 0;
volatile int32_t old_counter_r = 0;
volatile int speed_l = 0;
volatile int speed_r = 0;
volatile char state_l = 0;
volatile char state_r = 0;
volatile char old_state_l = 0;
volatile char old_state_r = 0;

volatile int32_t output_l = 0;
volatile int32_t output_r = 0;

// Control Variables
volatile float ref = 90;
volatile float K = 0.08;
volatile float vel = 0;
volatile float angle = 0;
volatile int32_t pos_r = 0, old_pos_r = 0;
volatile int32_t pos_l = 0, old_pos_l = 0;

ISR(PCINT1_vect){
	PORTC ^= (1 << RED_LED);
	state_l = old_state_l >> 2;
	if(ChannelA_l) state_l |= 0x04;
	if(ChannelB_l) state_l |= 0x08;
	switch (state_l) {
		case 0: case 5: case 10: case 15:
		break;
		case 1: case 7: case 8: case 14:
		counter_l++; break;
		case 2: case 4: case 11: case 13:
		counter_l--; break;
		case 3: case 12:
		counter_l += 2; break;
		default:
		counter_l -= 2; break;
	}
	old_state_l = state_l;
	PORTC ^= (1 << RED_LED);
}

ISR(PCINT0_vect){
	state_r = old_state_r >> 2;
	if(ChannelA_r) state_r |= 0x04;
	if(ChannelB_r) state_r |= 0x08;
	switch (state_r) {
		case 0: case 5: case 10: case 15:
		break;
		case 1: case 7: case 8: case 14:
		counter_r++; break;
		case 2: case 4: case 11: case 13:
		counter_r--; break;
		case 3: case 12:
		counter_r += 2; break;
		default:
		counter_r -= 2; break;
	}
	old_state_r = state_r;
}

ISR(TIMER0_COMPA_vect)
{
	sei();	// When an interrupt happens, global interrupt flag is disabled by hardware. So we must enable interrupt again for encoder
	PORTB ^= (1 << LED13);
	if(j < vector_size){
		pos_r = counter_r;
		pos_l = counter_l;
		//vel = (pos_r - old_counter_r)*pulses_to_rpm;
		angle = pos_l*0.6;	//360 graus/ 600 pulsos
		
		//pwm_aux = (int)(160*(sin(p*f[j]*i)));	//sine input
		//pwm_aux = 160;	//step input
		pwm_aux = (int)(30.3571*K*(ref - angle));		// 255/8 = 30.3571
		if(pwm_aux > 255)
			pwm_aux = 255;
		else if(pwm_aux < -255)
			pwm_aux = -255;
		if(i < trials[j]){				
			if(pwm_aux > 0){
				PORTD |= (1 << IN1_L);
				PORTD &= ~(1 << IN2_L);
				PORTD |= (1 << IN1_R);
				PORTB &= ~(1 << IN2_R);
				pwm_l = pwm_aux;
				//pwm_r = pwm_vector[i];
			}
			else{
				PORTD |= (1 << IN2_L);
				PORTD &= ~(1 << IN1_L);
				PORTB |= (1 << IN2_R);
				PORTD &= ~(1 << IN1_R);
				pwm_l = -pwm_aux;
				//pwm_r = -pwm_vector[i];
			}
			
			//sprintf(str, "%d %d\n\r", pwm_aux, counter_l);
			sprintf(str, "%2.2f %ld\n\r", f[j], pos_l);
			//sprintf(str, "%2.1f\n\r", vel);
			serial.send_str(str);			
			i++;
			k = 0;
		}
		else {
			if (k > 1000){	// Wait some time between trials
				i = 0;
				j++;
			}
			k++;
			pwm_l = 0;
			pwm_r = 0;
			counter_r = 0;
			counter_l = 0;
		}
	}	
	old_counter_l = pos_l;
	old_counter_r = pos_r;
	PORTB ^= (1 << LED13);
}

void timer0_init()
{
	// CTC mode on. The overflow period is controlled by OCR0A value
	TCCR0A |= (1 << WGM01);
	
	// Overflow period: 10 ms
	//OCR0A = 156;	// 10 ms
	OCR0A = 77;	// 5 ms
	
	// set up timer with prescaler = 1024.
	TCCR0B |= (1 << CS02) | (1 << CS00);
	
	// initialize counter
	TCNT0 = 0;
	
	// enable overflow interrupt
	TIMSK0 |= (1 << TOIE0) | (1 << OCIE0A);
	
	// enable global interrupts
	sei();
}

void pin_interrupt_init(){
	
	// connect channel A to PORTB1 and channel B to PORTB2 (for left motor). Enable pull up resistors
	//DDRD |= (0 << PORTD6) | (0 << PORTD7);
	PORTB |= (1 << PORTB1) | (1 << PORTB2);
	
	// connect channel A to PORTC0 and channel B to PORTC1 (for right motor). Enable pull up resistors
	//DDRC |= (0 << PORTC2) | (0 << PORTC3);
	PORTC |= (1 << PORTC0) | (1 << PORTC1);
	
	// enable pin change interrupt of PCINT0..7 pins and PCINT14..8 pins
	PCICR |= (1 << PCIE1) | (1 << PCIE0);
	
	// enable pin change interrupt of PINCT1 and PCINT 2
	PCMSK0 |= (1 << PCINT1) | (1 << PCINT2);
	
	// enable pin change interrupt of PINCT8 and PCINT 9
	PCMSK1 |= (1 << PCINT8) | (1 << PCINT9);
	
	// enable global interrupts
	sei();
}

void pwm_init(){
	// PORTD3 and PORTB3 as pwm outputs
	DDRD |= (1 << PORTD3);
	DDRB |= (1 << PORTB3);
	
	// Pins used to control the motor direction
	DDRD |= (1 << IN1_L) | (1 << IN2_L) | (1 << IN1_R) | (1 << STBY);
	DDRB |= (1 << IN2_R);
	
	// Timer 0 set to Compare Output Mode, Phase Correct PWM Mode
	TCCR2A |= (1 << COM2A1) | (1 << COM2B1) | (1 << WGM20);
	
	// PWM frequency = CLK/510/8 = 4 kHz
	TCCR2B |= (1 << CS21);
	
	// Duty cycle = OCR0/255 (valid for Phase Correct PWM Mode)
	OCR2A = 0;
	OCR2B = 0;
	
	DDRD |= (1 << STBY);
	PORTD |= (1 << STBY);
}

int main(void)
{
	pin_interrupt_init();
	timer0_init();
	pwm_init();
	serial.init();
	
	// Configure the RED_LED Pin
	DDRC |= (1 << RED_LED);
	DDRB |= (1 << LED13);
	
	// HBridge motor directions
	PORTD |= (1 << IN1_R);
	PORTB &= ~(1 << IN2_R);
	PORTD |= (1 << IN1_L);
	PORTD &= ~(1 << IN2_L);
	
	while(1)
	{
		
	}
}

//                           _______         _______
//               Pin1 ______|       |_______|       |______ Pin1
// negative <---         _______         _______         __      --> positive
//               Pin2 __|       |_______|       |_______|   Pin2

//	new	new	old	old
//	pin2	pin1	pin2	pin1	Result
//	----	----	----	----	------
//	0	0	0	0	no movement
//	0	0	0	1	+1
//	0	0	1	0	-1
//	0	0	1	1	+2  (assume pin1 edges only)
//	0	1	0	0	-1
//	0	1	0	1	no movement
//	0	1	1	0	-2  (assume pin1 edges only)
//	0	1	1	1	+1
//	1	0	0	0	+1
//	1	0	0	1	-2  (assume pin1 edges only)
//	1	0	1	0	no movement
//	1	0	1	1	-1
//	1	1	0	0	+2  (assume pin1 edges only)
//	1	1	0	1	-1
//	1	1	1	0	+1
//	1	1	1	1	no movement
// https://www.pjrc.com/teensy/td_libs_Encoder.html