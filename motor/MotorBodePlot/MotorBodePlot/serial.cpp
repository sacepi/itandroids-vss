/*
 * serial.cpp
 *
 * Created: 26/11/2014 00:16:24
 *  Author: igor
 */ 

#include "serial.h"
#include <avr/io.h>
#include <stdlib.h>

Serial::Serial(){
	
}

void Serial::init(){
	// enable transmitter
	UCSR0B |= (1 << TXEN0);
	
	// 8 bit data and 1 stop bit
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	
	// 115200 baud rate
	UBRR0L = 8;
}

void Serial::send_char(unsigned char ch){
	// wait until transmit buffer is empty
	while (! (UCSR0A & (1 << UDRE0)));
	
	// send ch
	UDR0 = ch;
}

void Serial::send_str(char * str){
	unsigned char i = 0;
	
	// since we do not know the size of the str we have
	// to check if the char about to be sent is the string
	// termination character \0
	while(str[i] != '\0'){
		send_char(str[i++]);
	}
}

void Serial::send_int32(int32_t n){
	unsigned char ch;
	
	if(n > 0){
		ch = n;
		send_char(ch);
		ch = ch >> 8;
		send_char(ch);
		ch = ch >> 8;
		send_char(ch);
		ch = ch >> 8;
		send_char(ch);
	}
	else{
		send_char('-');
		n=-n;
		ch = n;
		send_char(ch);
		ch = ch >> 8;
		send_char(ch);
		ch = ch >> 8;
		send_char(ch);
		ch = ch >> 8;
		send_char(ch);
	}
}