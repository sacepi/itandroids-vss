function cost = cost_fcn3(vel, Ts, times, guess)
    
    K = guess(1);
    a = guess(2);
        
    % Step response of the estimated 1st order tf
    s = tf('s');
    sys = (1-exp(-s*Ts))/(s*Ts) *K*a/(s+a);
    u = 4.5*ones(size(times));
    y = lsim(sys,u,times);

    % MSE
    cost = 1/length(vel)*(((y - vel)')*(y - vel));

end