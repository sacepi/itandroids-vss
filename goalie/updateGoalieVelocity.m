function v = updateGoalieVelocity(logGoalieVelocity,index,logDesiredGoalieVelocity,config)
	v = (config.a0*logDesiredGoalieVelocity(index) +...
             config.a1*logDesiredGoalieVelocity(index-1) + ...
             config.a2*logDesiredGoalieVelocity(index-2) + ...
             config.b1*logGoalieVelocity(index-1) + ...
             config.b2*logGoalieVelocity(index-2))/config.den;


	
	%v = logDesiredGoalieVelocity(index);
	if(v>config.maxVelocity)
		v = config.maxVelocity;
	end
	if(v<-config.maxVelocity)
		v = -config.maxVelocity;
	end

%	v = logDesiredGoalieVelocity(index);


%	Ts = 1e-3;
%	wn = 321.6701;
%	k = 9.3565e+06;
%	csi = 15.9256;
%	
%	% Input coeficients of the Difference Equation
%	a0 = k*Ts^2; % x[n] coeficient
%	a1 = 2*k*Ts^2; % x[n-1] coeficient
%	a2 = k*Ts^2; % x[n-2] coeficient
%
%	% Output Coeficients
%	b0 = 4+4*csi*wn*Ts+wn^2*Ts^2; % y[n] coeficient
%	b1 = 2*wn^2*Ts^2-8; % y[n-1] coeficient
%	b2 = 4-4*csi*wn*Ts+wn^2*Ts^2; % y[n-2] coeficient
%
%%	% Input test: Discrete Step Function
%%	x = ones(1000);
%%	x(1) = 0;
%%	x(2) = 0;
%%	y(1) = 0;
%%	y(2) = 0;
%%
%%	for n=3:length(x)
%  	v = (a1*logDesiredGoalieVelocity(index)+a1*logDesiredGoalieVelocity(index-1)+a2*logDesiredGoalieVelocity(index-2)-b1*logGoalieVelocity(index-1)-b2*logGoalieVelocity(index-2))/b0;
%%	end
%%
%%	n =1:length(x);
%%	figure(1)
%%	plot(n*Ts,y)
end
