function defenseFlag =  checkDefense(goaliePosition,ballPosition,config)
	if ((ballPosition(1) > (config.xMaxField - config.robotLength)) && ...
            (ballPosition(2) > (goaliePosition - config.robotLength/2)) &&
            (ballPosition(2) < (goaliePosition + config.robotLength/2)))
		defenseFlag = 1;
	else
		defenseFlag = 0;
	end
end
