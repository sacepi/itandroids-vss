function pos =  updateGoaliePosition(prevPos, v, config)
	pos = prevPos + v*config.Ts;
end
