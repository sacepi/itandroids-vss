function v = externalControl(logGoaliePosition,logGoalieVelocity,target,index,config);
	positionIndex = floor(index/config.nRate)*config.nRate - config.nDelay - 1;
	positionIndex = index - config.nDelay - 1;
	if positionIndex < 1
		positionIndex = 1;
	end;
	position = logGoaliePosition(positionIndex) + randn*config.visionError;	
	v = config.Kp*(target -  position);
end
