function out = simulateGoalie(config)

	%Generating initial Ball Position
	ballPosition(1) = config.xMinField + rand*(config.xMaxField - config.xMinField);
	ballPosition(2) = config.yMinField + rand*(config.yMaxField - config.yMinField);

	%Generating Ball Target
	ballTarget(1) = config.xGoalie;
	ballTarget(2) = config.yMinGoalie + rand*(config.yMaxGoalie - config.yMinGoalie);

	%Generating Ball Velocity;
	ballVelocity(1:2) =  (ballTarget - ballPosition)';
	ballVelocity(:) = ballVelocity(:)/norm(ballVelocity)*config.ballSpeed;
	
	%Generating Goalie Initial Position
	goaliePosition(1) =  config.xGoalie;
	goaliePosition(2) = config.yMinGoalie + rand*(config.yMaxGoalie - config.yMinGoalie);

	goalFlag = 0;
	defenseFlag = 0;
	goalieVelocity = 0;

	logGoaliePosition = zeros(1,config.maxIterations);
	logBallPosition = zeros(2,config.maxIterations);
	logGoalieVelocity = zeros(1,config.maxIterations);
	logDesiredGoalieVelocity = zeros(1,config.maxIterations);

	index =2;


	logBallPosition(:,1) = ballPosition;
	logBallPosition(:,2) = ballPosition;	
	defenseFlag = checkDefense(goaliePosition,ballPosition,config);
	logGoaliePosition(2) = goaliePosition(2);
	logGoaliePosition(1) = goaliePosition(2);


	while ~(goalFlag || defenseFlag)
		index = index+1;

		%Updating ball's and Goalie's states.
		ballPosition = updateBallPosition(ballPosition,ballVelocity,config);
		logBallPosition(:,index) = ballPosition;
		logDesiredGoalieVelocity(index) = externalControl(logGoaliePosition,logGoalieVelocity,ballTarget(2),index,config);
		logGoalieVelocity(index) = updateGoalieVelocity(logGoalieVelocity,index,logDesiredGoalieVelocity,config);
		logGoaliePosition(index) = updateGoaliePosition(logGoaliePosition(index-1),logGoalieVelocity(index),config);
		
		%Checking defenses and goals.
		defenseFlag = checkDefense(logGoaliePosition(index),logBallPosition(:,index),config);
		goalFlag = checkGoal(ballPosition,config);
	end

	if(config.showfig)
		%Drawing the field
		figure(1);
		clf();
		plot([config.xMinField,config.xMaxField,config.xMaxField,config.xMinField,config.xMinField],[config.yMinField,config.yMinField,config.yMaxField,config.yMaxField,config.yMinField],'LineWidth',config.fieldLineWidth,'k');
		axis([config.xMinField-0.05,config.xMaxField+0.05,config.yMinField-0.05,config.yMaxField+0.05]);
		hold on;
		plot([config.xMaxField,config.xMaxField],[config.yMinGoalie,config.yMaxGoalie],'LineWidth',config.fieldLineWidth,'g');

		plot(ones(1,index)*config.xMaxField,logGoaliePosition(1:index),'r');
		plot(logBallPosition(1,1:index),logBallPosition(2,1:index));
		plot(config.xMaxField,logGoaliePosition(index),'*r')
		

		figure(2)
		clf();
		hold on;
		plot((1:index)*config.Ts,logGoalieVelocity(1:index));
		plot((1:index)*config.Ts,logGoalieVelocity(1:index));
	end

	%disp(index)
	if defenseFlag
	%	disp('Defense');
		out = 1;
	else
	%	disp('Goal');
		out = 0;
	end
end
