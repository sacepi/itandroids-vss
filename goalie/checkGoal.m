function goalFlag = checkGoal(ballPosition,config)
	if ballPosition(1)>config.xMaxField
		goalFlag = 1;
	else goalFlag = 0;
	end
end
