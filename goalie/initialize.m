clear all;

%Simulation Parameters
config.nSamples = 100;
config.Ts = 1e-3;
config.showfig = 0;
config.maxIterations = 10000;
config.fieldLineWidth = 10;

%Ball parameters.
config.ballSpeed =2 ;

%Vision system parameters.
config.visionDelay = 0;
config.visionError = 0;
config.visionRate = 1/100;
config.Kp = 100;
Kp = [100];

%Goalie's parameters
config.robotLength = 0.075;
config.csi = 0.7;
config.wn = 2*pi*100;
config.maxVelocity = 0.5;

%Field Parameter
config.xMinField = 0;
config.xMaxField = 0.75;
config.yMinField = 0;
config.yMaxField = 1.3;
config.xGoalie = 0.75;
config.yMinGoalie = 0.45;
config.yMaxGoalie = 0.85;

%%Simulation
if config.Ts > config.visionRate
	config.Ts = config.visionRate;
end

for indexKp = 1:length(Kp)
	config.Kp = Kp(indexKp);


	%Calculating the 2nd order difference equation parameters
	T = config.Ts;
	wn = config.wn;
	csi = config.csi;
	k=1;
	
	config.den = 4+4*csi*wn*T+wn^2*T^2;
	
	config.a0 = k*wn^2*T^2;
	config.a1 = 2*config.a0;
	config.a2 = config.a0;
	
	config.b1 = 2*(4-wn^2*T^2);
	config.b2 = 4*csi*wn*T-4-wn^2*T^2;
	
	%Calculating the camera parameters
	config.nDelay = round(config.visionDelay/config.Ts);
	config.nRate  = round(config.visionRate/config.Ts);
	
	
	

	won(indexKp) = 0;
	tic
	for i = 1:config.nSamples
		won(indexKp) = won(indexKp)+simulateGoalie(config);
	end
	toc
	
	
	%disp('Percentual of defenses:');
	%disp(100*won/config.nSamples);
end
		disp(100*won/config.nSamples);
		%disp(config.nDelay);
